<?php

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class CampaignPosts_List_Table extends WP_List_Table
{
    var $campaign_id;
    var $posts;
    var $selected_posts;
    var $is_reminders;
    var $is_custom_post;

    var $is_ajaxed = false;

    var $table_name = 'ctctp_campaign_posts';
    var $is_filled_posts = false;

    function __construct($is_filled_posts = false, $is_reminders = false)
    {
        global $status, $page, $hook_suffix;

        parent::__construct(array(
            'singular' => 'campaign-post',
            'plural' => 'campaign-posts',
            'ajax' => true
        ));

        $this->screen = convert_to_screen('ctct-popup');

        $this->is_filled_posts = $is_filled_posts;

        $this->is_reminders = ($is_reminders == 1) ? 1 : 0;
    }

    /**
     * [REQUIRED] this is a default column renderer
     *
     * @param $item - row (key, value array)
     * @param $column_name - string (key)
     * @return HTML
     */
    function column_default($item, $column_name)
    {
        return $item[$column_name];
    }

    function column_post_content($item)
    {
        $limit = 100;

        if (strlen($item['post_content']) <= $limit) {
            return $item['post_content'];
        }
        else {
            return substr(stripslashes(strip_tags($item['post_content'])), 0, $limit) . '...';
        }
    }

    /**
     * [OPTIONAL] this is example, how to render column with actions,
     * when you hover row "Edit | Delete" links showed
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_post_title($item)
    {
        if ($this->is_filled_posts) {
            $actions = array(
                'edit' => sprintf('<a class="edit-campaign-post" data-post-id="%s" data-campaign-id="%s" href="javascript:void(0)">%s</a>', $item['id'], $this->campaign_id, __('Edit', 'ctct')),
                'delete' => sprintf('<a href="?page=%s" data-campaign_id=%s data-item_id=%s data-type=post>%s</a>', $_REQUEST['page'], $this->campaign_id, $item['id'], __('Delete', 'ctct')),
            );

            return sprintf('%s %s',
                $item['post_title'],
                $this->row_actions($actions)
            );
        }
        else {
            return $item['post_title'];
        }
    }

    /**
     * [REQUIRED] this is how checkbox column renders
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item)
    {
        if ($this->is_filled_posts) {
            return "";
        }
        if (in_array($item['id'], $this->posts)) {
            return '<input type="checkbox" checked disabled />';            
        }

        if (is_array($this->selected_posts) && in_array($item['id'], $this->selected_posts)) {
            return sprintf('<input class="item-available post-available" type="checkbox" name="post_id[]" value="%s" checked />', $item['id']);            
        }

        return sprintf(
            '<input class="item-available post-available" type="checkbox" name="post_id[]" value="%s" />',
            $item['id']
        );
    }

    function column_is_featured_post($item) {

        if ($item['is_featured_post']) {
            return '<span class="featured dashicons dashicons-yes" data-id='. $item['id'].'><span class="spinner"></span></span>';
        }

        // return '<span class="featured" data-id='. $item['id'].'><span class="spinner"></span></span>';
        return '<span class="featured" data-id='. $item['id'].'></span>';
    }

    function is_checked_item($id) {
       return in_array($id, $this->posts);
    }

    /**
     * [REQUIRED] This method return columns to display in table
     * you can skip columns that you do not want to show
     * like content, or description
     *
     * @return array
     */
    function get_columns()
    {
        if (!$this->is_filled_posts) {
            $columns = array('cb' => '');
        }
        else {
            $columns = array();
        }

        $columns['post_title'] = __('Title', 'ctct');
        $columns['post_content'] = __('Content', 'ctct');
        $columns['post_date'] = __('Created', 'ctct');
        if (!$this->is_reminders && !$this->is_ajaxed && !$this->is_custom_post) {
            $columns['is_featured_post'] = __('Is top event', 'ctct');
        }

        return $columns;
    }

    /**
     * [OPTIONAL] This method return columns that may be used to sort table
     * all strings in array - is column names
     * notice that true on name column means that its default sort
     *
     * @return array
     */
    function get_sortable_columns()
    {
        $sortable_columns = array(
            'post_title' => array('post_title', true),
            'post_content' => array('post_content', false),
            'post_date' => array('post_date', true)
        );
        if (!$this->is_reminders && !$this->is_ajaxed && !$this->is_custom_post) {
            $sortable_columns['is_featured_post'] = array('is_featured_post', true);
        }


        return $sortable_columns;
    }

    function display_tablenav( $which ) {
    
        if($which == 'top') {
            parent::display_tablenav($which);
        }
    }

    /**
     * [REQUIRED] This is the most important method
     *
     * It will get rows from database and prepare them to be showed in table
     */
    function prepare_items()
    {
        global $wpdb;
        $per_page = 10; 
        $total_pages = 1;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;
        $orderby = !empty( $_REQUEST['orderby'] ) && '' != $_REQUEST['orderby'] && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns())) ? $_REQUEST['orderby'] : 'post_date';
        $order = !empty( $_REQUEST['order'] ) && '' != $_REQUEST['order'] ? $_REQUEST['order'] : 'desc';

        if ($this->is_filled_posts) {
            $this->items = $this->select_campaign_items($paged, $orderby, $order);
        }
        else {
            $events = new CampaignEvent($this->campaign_id);
            $this->posts = $events->get_selected_ids();
            $this->items = $events->select_items($paged, $orderby, $order);

            $count_posts = $events->select_items_count();
            $total_pages = ceil($count_posts / $per_page);
        }

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => count($this->items), 
            'per_page' => count($this->items),
            'orderby'   => $orderby,
            'order'     => $order,            
            'total_pages' => $total_pages
        ));
    }

    function select_items($paged, $orderby, $order) {

        $per_page = 10;

        $args = array(  'posts_per_page' => $per_page,
                        'offset' => $paged * $per_page,
                        'post_type' => 'post',
                        'orderby' => $orderby,
                        'order' =>  $order );
        $recent_posts = get_posts( $args);

        $selected_posts = $this->get_selected_posts();

        $items = array();
        foreach($recent_posts as $post) {
            $item = array(
                    'id' => $post->ID, 
                    'post_title' => $post->post_title, 
                    'post_content' => $post->post_content,
                    'post_date' => $post->post_date,
                    'is_featured_post' => $post->is_featured_post
                    );

            $selected_item = $this->find_post($post->ID, $selected_posts);
            if ($selected_item != null) {
                $item['post_title'] = $selected_item['post_title'];
                $item['post_content'] = $selected_item['post_content'];
            }

            $items[] = $item;
        }

        return $items;
    }

    function select_campaign_items($paged, $orderby, $order)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . $this->table_name;

        if ($this->is_custom_post) {
            $posts = $wpdb->get_results($wpdb->prepare("SELECT post_id, post_title, post_content, post_date, is_reminder_post, is_featured_post FROM $table_name where campaign_id = %d and is_custom_post = 1 order by is_custom_post desc, $orderby $order", $this->campaign_id), ARRAY_A);
        }
        else {
            $posts = $wpdb->get_results($wpdb->prepare("SELECT post_id, post_title, post_content, post_date, is_reminder_post, is_featured_post FROM $table_name where campaign_id = %d and is_reminder_post = %d  and is_custom_post = 0 order by is_custom_post desc, $orderby $order", $this->campaign_id, $this->is_reminders), ARRAY_A);
        }

        $items = array();
        foreach ($posts as $key => $post) {
            $item = array(
                    'id' => $post['post_id'], 
                    'post_title' => $post['post_title'], 
                    'post_content' => $post['post_content'],
                    'post_date' => $post['post_date'],
                    'is_featured_post' => $post['is_featured_post']
                    );
            $items[] = $item;
        }

        return $items;
    }

    function find_post($id, $posts) {

        foreach($posts as $key => $post) {
            if ($post['post_id'] == $id) {
                return $post;
            }
        }

        return null;
    }

    function get_selected_ids()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . $this->table_name;

        $posts = $wpdb->get_results($wpdb->prepare("SELECT post_id FROM $table_name where campaign_id = %d", $this->campaign_id), ARRAY_A);

        $items = array();
        foreach ($posts as $key => $post) {
            $items[] = $post['post_id'];
        }

        return $items;
    }

    function get_selected_posts()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . $this->table_name;

        $posts = $wpdb->get_results($wpdb->prepare("SELECT post_id, post_title, post_content, is_reminder_post, is_featured_post FROM $table_name where campaign_id = %d", $this->campaign_id), ARRAY_A);

        return $posts;
    }

    function display() {
     
        if ($this->is_ajaxed) {
            wp_nonce_field( 'ajax-custom-list-nonce', '_ajax_custom_list_nonce' );
         
            echo '<input id="order" type="hidden" name="order" value="' . $this->_pagination_args['order'] . '" />';
            echo '<input id="orderby" type="hidden" name="orderby" value="' . $this->_pagination_args['orderby'] . '" />';
        }     

        parent::display();
    } 
    
    function ajax_response() {
     
        check_ajax_referer( 'ajax-custom-list-nonce', '_ajax_custom_list_nonce' );
     
        $this->prepare_items();
        $this->is_ajaxed = true;

//        extract( $this->_args );
        extract( $this->_pagination_args, EXTR_SKIP );
     
        ob_start();
        if ( ! empty( $_REQUEST['no_placeholder'] ) )
            $this->display_rows();
        else
            $this->display_rows_or_placeholder();
        $rows = ob_get_clean();
     
        ob_start();
        $this->print_column_headers();
        $headers = ob_get_clean();
     
        ob_start();
        $this->pagination('top');
        $pagination_top = ob_get_clean();
     
        $response = array( 'rows' => $rows );
        $response['pagination']['top'] = $pagination_top;
        $response['column_headers'] = $headers;

        if ( isset( $total_items ) )
            $response['total_items_i18n'] = sprintf( _n( '1 item', '%s items', $total_items ), number_format_i18n( $total_items ) );
     
        if ( isset( $total_pages ) ) {
            $response['total_pages'] = $total_pages;
            $response['total_pages_i18n'] = number_format_i18n( $total_pages );
        }
     
        die( json_encode( $response ) );
    }       
}
?>