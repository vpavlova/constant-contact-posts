<?php

class CampaignEvent {

    public $table_name;
    public $campaign_id;

	function __construct($campaign_id = null)
    {
        global $wpdb;
        $this->table_name = $wpdb->prefix . 'ctctp_campaign_events';

        if (isset($campaign_id)) {
            $this->campaign_id = $campaign_id;
        }
    }

    public function add() {

        global $wpdb;
        $message = '';
        $notice = '';
 
        if (isset($_REQUEST['campaign_event_id']) && $_REQUEST['campaign_event_id'] != '') {
            $result = $wpdb->update($this->table_name, 
                array('event_title'=> stripslashes($_REQUEST['event_title']), 
                    'event_date' => stripslashes($_REQUEST['event_date']),
                    'is_custom_event' => 1
                    ), 
                array('campaign_id' => $_REQUEST['id'], 
                    'event_id' => $_REQUEST['campaign_event_id']
                    ) 
            );                

            if ($result !== false){
                $message = __('Event was successfully updated', 'ctctp');
            }
            else {
                $notice = __('There was an error while updating event', 'ctctp');
            }
        }
        // insert custom event into campaign
        else {
            $result = $wpdb->insert($this->table_name, 
                array('event_title'=> stripslashes($_REQUEST['event_title']), 
                    'event_date' => stripslashes($_REQUEST['event_date']),
                    'campaign_id' => $_REQUEST['id'],
                    'event_id' => 0,
                    'is_custom_event' => 1
                    ) 
            );                

            if ($result !== false){
                $message = __('Event was successfully added', 'ctctp');
            }
            else {
                $notice = __('There was an error while adding event', 'ctctp');
            }
        }

        $arrResult = array(
                'result' => $result,
                'message' => $message,
                'notice' => $notice
            );

        return $arrResult;
    }   

    public function is_custom_event($campaign_id) {

        global $wpdb;

        $count = $wpdb->get_var($wpdb->prepare("SELECT count(id) FROM ".$this->table_name." where campaign_id = %d and is_custom_event = 1", $campaign_id));

        return $count > 0;
    }

    public function delete($campaign_id, $event_id) {

        global $wpdb;
        $campaigns_table_name = $wpdb->prefix . 'ctctp_campaigns';

        $result = $wpdb->delete($this->table_name, array( 'campaign_id' => $campaign_id, 'event_id' => $event_id ));

        // check count of events campaign contains, unset filled events flag for campaign if there are not any campaign events
        $row_count = $wpdb->get_var($wpdb->prepare("SELECT count(id) FROM ".$this->table_name." where campaign_id = %d", $campaign_id));
        if ($row_count == 0) {
            $wpdb->query($wpdb->prepare("UPDATE $campaigns_table_name SET is_filled_events = 0 WHERE ID = %d", $campaign_id));
        }
    }

    public function select_items($paged, $orderby, $order) {

        $per_page = 10;
        $limit = '' . ($paged * $per_page) . ', ' . $per_page;

        $items = array();

        if ($orderby == 'post_date') {
            $orderby = 'Datetime.DTT_EVT_start';
        }

        $recent_posts = EE_Registry::instance()->load_model( 'Event' )->get_all( array(
                 array( 
//                        'status' => 'publish', 
                        'Datetime.DTT_EVT_start' => array( '>=', date( current_time( 'mysql' ) ), 
                    )
                ),
                 'limit' => $limit,
                 'order_by' => $orderby,
                 'order' => $order,
                 'group_by' => 'EVT_ID'
                ));

//        $selected_posts = $this->get_selected_posts();

        foreach( $recent_posts as $event ) {
            $item = array (
                    'id' => $event->get( 'EVT_ID' ),
                    'post_title' => $event->get( 'EVT_name' ),
                    'post_date' => $event->primary_datetime()->start_date_and_time()
                );

            $post = get_post($item['id']);
            $item['post_content'] =  $post->post_content;
            $item['is_reminder_post'] = 0;
            $item['is_featured_post'] = $post->is_featured_post;

            // $selected_item = $this->find_post($post->ID, $selected_posts);
            // if ($selected_item != null) {
            //     $item['post_title'] = $selected_item['post_title'];
            //     $item['post_content'] = $selected_item['post_content'];
            // }

            $items[] = $item;
        }

        return $items;        
    }

    public function select_campaign_items($paged, $orderby, $order) {

        global $wpdb;
        $table_name = $wpdb->prefix . "ctctp_campaign_posts";

        $posts = $wpdb->get_results($wpdb->prepare("SELECT post_id, post_title, post_content, post_date FROM $table_name where campaign_id = %d order by is_custom_post desc, $orderby $order", $this->campaign_id), ARRAY_A);

        $items = array();
        foreach ($posts as $key => $post) {
            $item = array(
                    'id' => $post['post_id'], 
                    'post_title' => $post['post_title'], 
                    'post_content' => $post['post_content'],
                    'post_date' => $post['post_date']
                    );
            $items[] = $item;
        }

        return $items;
    }

    public function get_selected_ids()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "ctctp_campaign_posts";

        $event_ids = $wpdb->get_results($wpdb->prepare("SELECT post_id FROM $table_name where campaign_id = %d", $this->campaign_id), ARRAY_A);

        $items = array();
        foreach ($event_ids as $key => $post) {
            $items[] = $post['post_id'];
        }

        return $items;
    }

    public function get_featured_id() {
        global $wpdb;
        $table_name = $wpdb->prefix . "ctctp_campaign_posts";

        $event = $wpdb->get_row($wpdb->prepare("SELECT post_id FROM $table_name where campaign_id = %d and is_featured_post = 1", $this->campaign_id), ARRAY_A);

        if ($event) {
            return $event['post_id'];
        }

        return 0;        
    }

    public function select_items_count() 
    {
        $recent_posts = EE_Registry::instance()->load_model( 'Event' )->get_all( array(
                 array( 
//                        'status' => 'publish', 
                        'Datetime.DTT_EVT_start' => array( '>=', date( current_time( 'mysql' ) ), 
                    )
                )
                ));
        return count($recent_posts);
    }

    function find_post($id, $posts) {

        foreach($posts as $key => $post) {
            if ($post['post_id'] == $id) {
                return $post;
            }
        }

        return null;
    }

    function get_selected_posts()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "ctctp_campaign_posts";

        $posts = $wpdb->get_results($wpdb->prepare("SELECT post_id, post_title, post_content FROM $table_name where campaign_id = %d", $this->campaign_id), ARRAY_A);

        return $posts;
    }   

    public function select_events($event_ids) {

        $recent_posts = EE_Registry::instance()->load_model( 'Event' )->get_all( array(
                 array( 
//                        'status' => 'publish', 
                        'Datetime.DTT_EVT_start' => array( '>=', date( current_time( 'mysql' ) ), 
                    )
                ),
                 'group_by' => 'EVT_ID'
                ));

        foreach( $recent_posts as $event ) {
            $event_id = $event->get( 'EVT_ID' ); 
            if (in_array($event_id, $event_ids)) {
                $item = array (
                        'id' => $event_id,
                        'post_title' => $event->get( 'EVT_name' ),
                        'post_date' => $event->primary_datetime()->start_date_and_time()
                    );

                $post = get_post($item['id']);
                $item['post_content'] =  $post->post_content;

                $items[] = $item;
            }
        }

        return $items;        
    }
}

?>