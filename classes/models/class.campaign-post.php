<?php

class CampaignPost {

    public $table_name;

	function __construct()
    {
        global $wpdb;
        $this->table_name = $wpdb->prefix . 'ctctp_campaign_posts';
    }

    public function add() {

        global $wpdb;
        $message = '';
        $notice = '';
 
        if (isset($_REQUEST['campaign_post_id']) && $_REQUEST['campaign_post_id'] != '') {
            $result = $wpdb->update($this->table_name, 
                array('post_title'=> stripslashes($_REQUEST['post_title']), 
                    'post_content' => stripslashes($_REQUEST['editor_post_content'])
                    ), 
                array('campaign_id' => $_REQUEST['id'], 
                    'post_id' => $_REQUEST['campaign_post_id']
                    ) 
            );                

            if ($result !== false){
                $message = __('Post was successfully updated', 'ctctp');
            }
            else {
                $notice = __('There was an error while updating post', 'ctctp');
            }
        }
        // insert custom post into campaign
        else {
            $result = $wpdb->insert($this->table_name, 
                array('post_title'=> stripslashes($_REQUEST['post_title']), 
                    'post_content' => stripslashes($_REQUEST['editor_post_content']),
                    'campaign_id' => $_REQUEST['id'],
                    'post_id' => 0,
                    'post_date' => date('Y-m-d H:i:s'),
                    'is_custom_post' => 1,
                    'is_reminder_post' => $_REQUEST['is_reminder_post']
                    ) 
            );                

            if ($result !== false){
                $message = __('Post was successfully added', 'ctctp');
            }
            else {
                $notice = __('There was an error while adding post', 'ctctp');
            }
        }

        $arrResult = array(
        		'result' => $result,
        		'message' => $message,
        		'notice' => $notice
        	);

        return $arrResult;
    }	

    public function is_custom_post($campaign_id) {

    	global $wpdb;

        $count = $wpdb->get_var($wpdb->prepare("SELECT count(id) FROM ".$this->table_name." where campaign_id = %d and is_custom_post = 1", $campaign_id));

        return $count > 0;
    }

    public function delete($campaign_id, $post_id) {

        global $wpdb;
        $campaigns_table_name = $wpdb->prefix . 'ctctp_campaigns';

        $result = $wpdb->delete($this->table_name, array( 'campaign_id' => $campaign_id, 'post_id' => $post_id ));

        // check count of posts campaign contains, unset filled posts flag for campaign if there are not any campaign posts
        $row_count = $wpdb->get_var($wpdb->prepare("SELECT count(id) FROM ".$this->table_name." where campaign_id = %d", $campaign_id));
        if ($row_count == 0) {
            $wpdb->query($wpdb->prepare("UPDATE $campaigns_table_name SET is_filled_posts = 0 WHERE ID = %d", $campaign_id));
        }
    }
}

?>