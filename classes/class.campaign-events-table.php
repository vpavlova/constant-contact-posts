<?php

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class CampaignEvents_List_Table extends WP_List_Table
{
    var $campaign_id;
    var $posts;
    var $selected_posts;

    var $is_ajaxed = false;

    var $edit_action = '';

    var  $orderby;
    var $order;

    var $table_name = 'ctctp_campaign_events';
    var $is_filled_events = false;

    function __construct($is_filled_events = false)
    {
        global $status, $page, $hook_suffix;

        parent::__construct(array(
            'singular' => 'select-post',
            'plural' => 'select-posts',
        ));

        $this->screen = convert_to_screen('ctct-popup');

        $this->is_filled_events = $is_filled_events;
    }

    function column_default($item, $column_name)
    {
        return $item[$column_name];
    }

    /**
     * [OPTIONAL] this is example, how to render column with actions,
     * when you hover row "Edit | Delete" links showed
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_name($item)
    {
        if ($this->is_filled_events) {
            $actions = array();

            return sprintf('%s %s',
                $item['name'],
                $this->row_actions($actions)
            );
        }
        else {
            return $item['name'];
        }
    }

    /**
     * [REQUIRED] this is how checkbox column renders
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item)
    {
        if ($this->is_filled_events) {
            return '<input type="checkbox" checked disabled />';
        }

        if (in_array($item['id'], $this->posts)) {
            return '<input type="checkbox" checked disabled />';            
        }

        if (is_array($this->selected_posts) && in_array($item['id'], $this->selected_posts)) {
            return sprintf('<input class="item-available event-available" type="checkbox" name="event_id[]" value="%s" checked />', $item['id']);            
        }

        return sprintf(
            '<input class="item-available event-available" type="checkbox" name="event_id[]" value="%s" />',
            $item['id']
        );
    }

    /**
     * [REQUIRED] This method return columns to display in table
     * you can skip columns that you do not want to show
     * like content, or description
     *
     * @return array
     */
    function get_columns()
    {
        if (!$this->is_filled_events) {
//            $columns = array('cb' => '<input type="checkbox"/>');
            $columns = array('cb' => '');
        }
        else {
            $columns = array();
        }

        $columns['name'] = __('Event', 'ctct');
        $columns['event_date'] = __('Event date', 'ctct');

        return $columns;
    }

    /**
     * [OPTIONAL] This method return columns that may be used to sort table
     * all strings in array - is column names
     * notice that true on name column means that its default sort
     *
     * @return array
     */
    function get_sortable_columns()
    {
        $sortable_columns = array(
            'name' => array('name', true),
            'event_date' => array('event_date', true),
        );
        return $sortable_columns;
    }

    /**
     * [REQUIRED] This is the most important method
     *
     * It will get rows from database and prepare them to be showed in table
     */
    function prepare_items()
    {
        $per_page = 10; 
        $total_pages = 1;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;;
        $this->orderby = !empty( $_REQUEST['orderby'] ) && '' != $_REQUEST['orderby'] && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns())) ? $_REQUEST['orderby'] : 'event_date';
        $this->order = !empty( $_REQUEST['order'] ) && '' != $_REQUEST['order'] ? $_REQUEST['order'] : 'asc';

        $this->items = $this->select_items($paged, $this->orderby, $this->order);

        $count_posts = count($this->items);
        $total_pages = 1;

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => count($this->items), 
            'per_page' => count($this->items),
            'orderby'   => $this->orderby,
            'order'     => $this->order,            
            'total_pages' => $total_pages
        ));
    }

    function select_items($paged, $orderby, $order) {

        $per_page = 10;
        $limit = '' . ($paged * $per_page) . ', ' . $per_page;

        $items = array();

        if ($orderby == 'event_date') {
            $orderby = 'Datetime.DTT_EVT_start';
        }

        $recent_posts = EE_Registry::instance()->load_model( 'Event' )->get_all( array(
                 array( 
//                        'status' => 'publish', 
                        'Datetime.DTT_EVT_start' => array( '>=', date( current_time( 'mysql' ) ), 
                    )
                ),
                 'order_by' => $orderby,
                 'order' => $order,
                 'group_by' => 'EVT_ID'
                ));

        foreach( $recent_posts as $event ) {
            $item = array (
                    'id' => $event->get( 'EVT_ID' ),
                    'name' => $event->get( 'EVT_name' ),
                    'event_date' => $event->primary_datetime()->start_date_and_time()
                );
            $items[] = $item;
        }

        return $items;
    }

    function select_campaign_items($paged, $orderby, $order) {

        global $wpdb;
        $table_name =  $wpdb->prefix . $this->table_name;

        $event_ids = $wpdb->get_results($wpdb->prepare("SELECT event_id, is_custom_event FROM $table_name where campaign_id = %d", $this->campaign_id), ARRAY_A);

        $items = array();

        if (count($event_ids) > 0) {
            EE_Registry::instance()->load_helper( 'Event_View' );
            foreach ($event_ids as $key => $event) {
                if ($event['is_custom_event']) {
                    continue;
                }

                $event = EEH_Event_View::get_event($event['event_id']);
                $item = array (
                        'id' => $event->get( 'EVT_ID' ),
                        'name' => $event->get( 'EVT_name' ),
                        'event_date' => $event->primary_datetime()->start_date_and_time()
                    );
                $items[] = $item;
            }
        }

        // sort events by date
        usort($items, function($event1, $event2) {
            if ($event1['event_date'] == $event2['event_date']) {
                return 0;
            }
            return (strtotime($event1['event_date']) < strtotime($event2['event_date'])) ? -1 : 1;        
        });

        $custom_events = $wpdb->get_results($wpdb->prepare("SELECT event_id, event_title, event_date FROM $table_name where campaign_id = %d and is_custom_event = 1 order by $orderby $order", $this->campaign_id), ARRAY_A);

        foreach ($custom_events as $key => $event) {
            $item = array(
                    'id' => $event['event_id'], 
                    'name' => $event['event_title'], 
                    'event_date' => $event['event_date']
                    );
            array_unshift($items, $item);
        }

        return $items;
    }

    function get_selected_ids()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . $this->table_name;

        $event_ids = $wpdb->get_results($wpdb->prepare("SELECT event_id FROM $table_name where campaign_id = %d", $this->campaign_id), ARRAY_A);

        $items = array();
        foreach ($event_ids as $key => $post) {
            $items[] = $post['event_id'];
        }

        return $items;
    }

    function select_items_count() 
    {
        $recent_posts = EE_Registry::instance()->load_model( 'Event' )->get_all( array(
                 array( 
//                        'status' => 'publish', 
                        'Datetime.DTT_EVT_start' => array( '>=', date( current_time( 'mysql' ) ), 
                    )
                )
                ));
        return count($recent_posts);
    }

    function display() {
     
        if ($this->is_ajaxed) {
            wp_nonce_field( 'ajax-custom-list-nonce', '_ajax_custom_list_nonce' );
         
            echo '<input id="order" type="hidden" name="order" value="' . $this->_pagination_args['order'] . '" />';
            echo '<input id="orderby" type="hidden" name="orderby" value="' . $this->_pagination_args['orderby'] . '" />';
//            echo '<input type="hidden" name="page" value="'. ((isset($_REQUEST['page'])) ? $_REQUEST['page'] : 0) . '"/>';
        } 
     
        parent::display();
    }    

    function ajax_response() {
     
        check_ajax_referer( 'ajax-custom-list-nonce', '_ajax_custom_list_nonce' );
     
        $this->prepare_items();
        $this->is_ajaxed = true;
     
//        extract( $this->_args );
        extract( $this->_pagination_args, EXTR_SKIP );
        // $this->set_pagination_args(array(
        //     'total_items'   => $this->_pagination_args['total_items'], 
        //     'per_page'      => $this->_pagination_args['per_page'],
        //     'orderby'       => $this->_pagination_args['orderby'],
        //     'order'         => $this->_pagination_args['order'],            
        //     'total_pages'   => $this->_pagination_args['total_pages']
        // ));

        ob_start();
        if ( ! empty( $_REQUEST['no_placeholder'] ) )
            $this->display_rows();
        else
            $this->display_rows_or_placeholder();
        $rows = ob_get_clean();
     
        ob_start();
        $this->print_column_headers();
        $headers = ob_get_clean();
     
        ob_start();
        $this->pagination('top');
        $pagination_top = ob_get_clean();
     
        $response = array( 'rows' => $rows );
        $response['pagination']['top'] = $pagination_top;
        $response['column_headers'] = $headers;

        if ( isset( $total_items ) )
            $response['total_items_i18n'] = sprintf( _n( '1 item', '%s items', $total_items ), number_format_i18n( $total_items ) );
     
        if ( isset( $total_pages ) ) {
            $response['total_pages'] = $total_pages;
            $response['total_pages_i18n'] = number_format_i18n( $total_pages );
        }
     
        die( json_encode( $response ) );
    }       
}
?>