<?php

class ConstantContactsPosts {

    private $menu_slug = 'constant-contacts-posts';

    private static $instance = NULL;

    function __construct() {

        if(!defined('CTCTPOSTS_PLUGIN_DIR')) {

            define( 'CTCTPOSTS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
            define( 'CTCTPOSTS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

            add_action('plugins_loaded', array($this, 'init'), 1);
        } else {
            $this->init();
        }

        require_once(CTCTPOSTS_PLUGIN_DIR . 'classes/models/class.campaign-event.php');
        require_once(CTCTPOSTS_PLUGIN_DIR . 'classes/models/class.campaign-post.php');

        require_once(CTCTPOSTS_PLUGIN_DIR . 'classes/class.campaigns-table.php');
//        require_once(CTCTPOSTS_PLUGIN_DIR . 'classes/class.select-posts.php');
        require_once(CTCTPOSTS_PLUGIN_DIR . 'classes/class.campaign-posts-table.php');
        require_once(CTCTPOSTS_PLUGIN_DIR . 'classes/class.campaign-events-table.php');
    }

    static function getInstance() {

        if(empty(self::$instance)){
            self::$instance = new ConstantContactsPosts();
        }

        return self::$instance;
    }

    public function init() {

        add_action( 'admin_menu', array( $this, 'admin_menu' ));
        add_action( 'admin_init', array( $this, 'register_settings') );

        add_action( 'admin_enqueue_scripts', array( $this, 'custom_admin_scripts') );

        add_action( 'wp_ajax_ctctp_select_events', array( $this, 'select_events_callback') );
        add_action( 'wp_ajax_ctctp_select_posts', array( $this, 'select_posts_callback') );
        add_action( 'wp_ajax_ctctp_select_events_ajax', array( $this, 'select_events_ajax_callback') );
        add_action( 'wp_ajax_ctctp_select_posts_ajax', array( $this, 'select_posts_ajax_callback') );
        add_action( 'wp_ajax_ctctp_edit_post_ajax', array( $this, 'edit_post_ajax_callback') );
        add_action( 'wp_ajax_ctctp_edit_event_ajax', array( $this, 'edit_event_ajax_callback') );

        add_action( 'wp_ajax_ctctp_delete_item_ajax', array( $this, 'delete_item_ajax_callback') );

        add_action( 'wp_ajax_ctctp_send_campaign', array( $this, 'send_campaign_callback') );

        add_action('wp_ajax_ctctp_set_top_event', array( $this, 'set_top_event_callback') );
        add_action( 'wp_ajax_ctctp_set_reminder_event_ajax', array( $this, 'set_reminder_event_ajax_callback') );        
    }

    public function admin_menu() {

        add_menu_page( __('CC Posts', 'ctctp'), __('CC Posts', 'ctctp'), 'manage_options', $this->menu_slug, array( $this, 'display_page' ), CTCTPOSTS_PLUGIN_URL . '/images/constant-contact-admin-icon.png' );
        add_submenu_page($this->menu_slug, __('Campaigns', 'ctctp'), __('Campaigns', 'ctctp'), 'manage_options', $this->menu_slug . '/campaigns', array( $this, 'list_campaigns') );
        add_submenu_page($this->menu_slug, __('Add new', 'ctctp'), __('Add new', 'ctctp'), 'manage_options', $this->menu_slug . '/edit-campaign', array( $this, 'edit_campaign') );
        add_submenu_page($this->menu_slug, __('Settings', 'ctctp'), __('Settings', 'ctctp'), 'manage_options', $this->menu_slug . '/settings', array( $this, 'edit_settings') );
        remove_submenu_page($this->menu_slug, $this->menu_slug);
    }

    public function custom_admin_scripts() {

        wp_enqueue_style( 'admin-css', plugins_url('/css/ctctp-css.css', __FILE__), array(), null, 'all' );
        wp_enqueue_script( 'admin-init', plugins_url('/scripts/ctctp-js.js', __FILE__) , array('jquery'), null, true );
        wp_enqueue_script( 'admin-init1', plugins_url('/scripts/related-posts.js', __FILE__) , array('jquery'), null, true );
        wp_enqueue_script( 'admin-init2', plugins_url('/scripts/wp-table.js', __FILE__) , array('jquery'), null, true );
    }

    public function edit_settings() {

        include(CTCTPOSTS_PLUGIN_DIR.'views/settings.php');
    }

    public function list_campaigns() {

        global $wpdb;
        $campaigns_table_name = $wpdb->prefix . 'ctctp_campaigns';
        $campaign_events_table_name = $wpdb->prefix . 'ctctp_campaign_events';
        $campaign_posts_table_name = $wpdb->prefix . 'ctctp_campaign_posts';

        if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete') {

            $id = $_REQUEST['id'];

            $wpdb->delete($campaign_events_table_name, array( 'campaign_id' => $id ) );
            $wpdb->delete($campaign_posts_table_name, array( 'campaign_id' => $id ) );
            $wpdb->delete($campaigns_table_name, array( 'id' => $id ) );
        }


        $table = new Campaigns_List_Table();
        $table->prepare_items();

        $menu_slug = $this->menu_slug;

        $table->edit_action = $menu_slug . '/edit-campaign';

        $message = '';
        if ('delete' === $table->current_action()) {
            $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'ctctp'), count($_REQUEST['id'])) . '</p></div>';
        }

        include(CTCTPOSTS_PLUGIN_DIR.'views/campaigns.php');
    }

    public function edit_campaign() {

        global $wpdb;
        $campaigns_table_name = $wpdb->prefix . 'ctctp_campaigns';
        $campaign_posts_table_name = $wpdb->prefix . 'ctctp_campaign_posts';

        $message = '';
        $notice = '';

        $default = array(
            'id' => 0,
            'name' => '',
            'introduction' => '',
            'creation_date' => date('Y-m-d H:i:s')
        );

        $nonce = basename(__FILE__);

        if (isset($_REQUEST['_ajax_nonce']) && wp_verify_nonce($_REQUEST['_ajax_nonce'], 'find-posts')) {

            $item = shortcode_atts($default, $_REQUEST);

            // edit post in campaign
            if (isset($_REQUEST['edit-posts-submit'])) {

                $campaignPost = new CampaignPost();
                $result = $campaignPost->add();

                if ($result['result']) {
                    $message = $result['message'];
                }
                else {
                    $notice = $result['notice'];
                }
            }
            // add posts to campaign
            else if (isset($_REQUEST['post_id']) || isset($_REQUEST['posts_added'])) {
                if (isset($_REQUEST['post_id']) && isset($_REQUEST['posts_added'])) {
                    $posts_to_add = array_unique(array_merge($_REQUEST['post_id'], $_REQUEST['posts_added']));
                }
                else if (isset($_REQUEST['post_id'])) {
                    $posts_to_add = $_REQUEST['post_id'];    
                }
                else {
                    $posts_to_add = $_REQUEST['posts_added'];    
                }

                $result = $this->add_posts($item['id'], $posts_to_add, $_REQUEST['is_reminder_post']);
                if ($result) {
                    $message = __('Posts were successfully added', 'ctctp');
                } else {
                    $notice = __('There was an error while adding posts', 'ctctp');
                }

//                $wpdb->query($wpdb->prepare("UPDATE ".$campaigns_table_name." SET is_filled_posts = 1 WHERE ID = %d", $item['id']));
                $update_fields = array();
                if (!$_REQUEST['is_reminder_post']) {
                    $update_fields = array('is_filled_posts' => 1);
                }
                else {
                    $update_fields = array('is_filled_reminders' => 1);
                }

                $wpdb->update($campaigns_table_name,
                    $update_fields,
                    array('id' => $item['id'])
                );
            }
        }
        else if (isset($_REQUEST['nonce']) && wp_verify_nonce($_REQUEST['nonce'], $nonce)) {

            $item = shortcode_atts($default, $_REQUEST);
//            $item_valid = custom_table_example_validate_person($item);
            $item_valid = true;

            if ($item_valid === true) {
                if ($item['id'] == 0) { // add campaign
                    $result = $wpdb->insert($campaigns_table_name, $item);

                    $item['id'] = $wpdb->insert_id;
                    if ($result) {
                        $message = __('Item was successfully saved', 'ctctp');
                    } else {
                        $notice = __('There was an error while saving item', 'ctctp');
                    }

                    $item['is_filled_posts'] = 0;
                    $item['is_filled_reminders'] = 0;

                } else { // update campaign details

                    if (isset($_REQUEST['id'])) {
                        $dbItem = $wpdb->get_row($wpdb->prepare("SELECT * FROM $campaigns_table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
                     }

                    $result = $wpdb->update($campaigns_table_name, $item, array('id' => $item['id']));
                    if ($result) {
                        $message = __('Item was successfully updated', 'ctctp');
                    } else {
                        $notice = __('There was an error while updating item', 'ctctp');
                    }
                }
            } else {
                $notice = $item_valid;
            }
        }

        if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $campaigns_table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
//            print_r($item);
        }

        if (!isset($item)) {
            $item = $default;
            $item['is_filled_posts'] = false;
            $item['is_filled_reminders'] = false;
        }

        // show left column events
        $events_table = new CampaignEvents_List_Table(true);
        $events_table->campaign_id = $item['id'];
        $events_table->prepare_items();

        // show new added events
        if (isset($item["is_filled_posts"]) && (bool)($item["is_filled_posts"] + 0)) {
            $posts_table = new CampaignPosts_List_Table(true);
            $posts_table->campaign_id = $item['id'];
            $posts_table->prepare_items();
        }

        // show reminders
        $is_custom_post = false;
        if (isset($item["is_filled_reminders"]) && (bool)($item["is_filled_reminders"] + 0)) {
            $reminders_table = new CampaignPosts_List_Table(true, 1);
            $reminders_table->campaign_id = $item['id'];
            $reminders_table->prepare_items();

            $is_custom_event = true;
            // check if custom post is added
            $campaignPost = new CampaignPost();
            $is_custom_post = $campaignPost->is_custom_post($item['id']);
        }

        // show custom post
        $is_custom_event = true;
        // check if custom post is added
        $campaignPost = new CampaignPost();
        $is_custom_post = $campaignPost->is_custom_post($item['id']);

        if ($is_custom_post) {
            $custom_post_table = new CampaignPosts_List_Table(true);
            $custom_post_table->is_custom_post = true;
            $custom_post_table->campaign_id = $item['id'];
            $custom_post_table->prepare_items();
        }

        add_action( 'admin_footer', 'wp_print_media_templates' );
        wp_enqueue_media();

        $menu_slug = $this->menu_slug;
        include(CTCTPOSTS_PLUGIN_DIR.'views/edit-campaign.php');
    }

    private function add_posts($campaign_id, $post_ids, $is_reminder_post) {

        global $wpdb;
        $campaign_posts_table_name = $wpdb->prefix . 'ctctp_campaign_posts';

        $campaignEvents = new CampaignEvent($campaign_id);
        $existed_post_ids = $campaignEvents->get_selected_ids();

        $ids = array();
        foreach ($post_ids as $key => $post_id) {
            if (!in_array($post_id, $existed_post_ids)) {
                $ids[] = $post_id;
            }
        }

        if (count($ids) > 0) {
            $campaign_posts = $campaignEvents->select_events($ids);
            foreach($campaign_posts as $key => $post ) {

                $postDate = DateTime::createFromFormat('F d, Y h:i a', $post['post_date']);
                
                $item = array(
                        'campaign_id' => $campaign_id,
                        'post_id' => $post['id'],
                        'post_title' => $post['post_title'],
                        'post_content' => $post['post_content'],
                        'post_date' => $postDate->format('Y-m-d h:i:s'),
                        'is_reminder_post' => $is_reminder_post
                        );

                $result = $wpdb->insert($campaign_posts_table_name, $item);
                if (!$result) {
                    return $result;
                }
            }
        }

        return true;
    }

    private function add_events($campaign_id, $event_ids) {

        global $wpdb;
        $campaign_events_table_name = $wpdb->prefix . 'ctctp_campaign_events';

        $events_table = new CampaignEvents_List_Table(true);
        $events_table->campaign_id = $campaign_id;
        $existed_event_ids = $events_table->get_selected_ids();

        if (is_array($event_ids)) {
            foreach($event_ids as $key => $event_id) {

                if (!in_array($event_id, $existed_event_ids)) {

                    $item = array(
                        'campaign_id' => $campaign_id,
                        'event_id' => $event_id
                    );

                    $result = $wpdb->insert($campaign_events_table_name, $item);
                    if (!$result) {
                        return $result;
                    }
                }
            }
        }

        return true;
    }

    public function delete_item_ajax_callback() {

        global $wpdb;
        $campaigns_table_name = $wpdb->prefix . 'ctctp_campaigns';

        $campaign_id = $_REQUEST['campaign_id'];
        $post_id = $_REQUEST['item_id'];
        $type = $_REQUEST['type'];

        if($type == 'post') {
            $campaignPost = new CampaignPost();
            $campaignPost->delete($campaign_id, $post_id);
        }
        elseif($type == 'event') {
            $campaignEvent = new CampaignEvent();
            $campaignEvent->delete($campaign_id, $post_id);
        }
    }

    public function select_events_callback() {

        $table = new CampaignEvents_List_Table();
        if (isset($_REQUEST['id'])) {
            $table->campaign_id = $_REQUEST['id'];
        }
        $table->prepare_items();
        $table->is_ajaxed = true;

        $menu_slug = $this->menu_slug;

        if(isset($_REQUEST['items_added']) && is_array($_REQUEST['items_added'])) {
            $table->selected_posts = $_REQUEST['items_added'];
        }

        $message = '';
        if ('delete' === $table->current_action()) {
            $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'ctctp'), count($_REQUEST['id'])) . '</p></div>';
        }

        include(CTCTPOSTS_PLUGIN_DIR.'views/select-events.php');

        wp_die();
    }

    public function select_posts_callback() {

        $table = new CampaignPosts_List_Table();
        if (isset($_REQUEST['id'])) {
            $table->campaign_id = $_REQUEST['id'];
        }
        $table->is_ajaxed = true;
        $table->prepare_items();

        $menu_slug = $this->menu_slug;

        if(isset($_REQUEST['items_added']) && is_array($_REQUEST['items_added'])) {
            $table->selected_posts = $_REQUEST['items_added'];
        }

        $message = '';
        if ('delete' === $table->current_action()) {
            $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'ctctp'), count($_REQUEST['id'])) . '</p></div>';
        }

        include(CTCTPOSTS_PLUGIN_DIR.'views/select-posts.php');

        wp_die();
    }

    public function select_events_ajax_callback() {

        $table = new CampaignEvents_List_Table();
        if (isset($_REQUEST['id'])) {
            $table->campaign_id = $_REQUEST['id'];
        }
        $table->prepare_items();

        if(isset($_REQUEST['items_added']) && is_array($_REQUEST['items_added'])) {
            $table->selected_posts = $_REQUEST['items_added'];
        }

        $table->ajax_response();
    }

    public function select_posts_ajax_callback() {

        $table = new CampaignPosts_List_Table();
        if (isset($_REQUEST['id'])) {
            $table->campaign_id = $_REQUEST['id'];
        }
        $table->prepare_items();

        if(isset($_REQUEST['items_added']) && is_array($_REQUEST['items_added'])) {
            $table->selected_posts = $_REQUEST['items_added'];
        }

        $table->ajax_response();
    }

    public function send_campaign_callback() {

        global $wpdb;
        global $campaign_item;

        $campaigns_table_name = $wpdb->prefix . 'ctctp_campaigns';
        $campaign_events_table_name = $wpdb->prefix . 'ctctp_campaign_events';
        $campaign_posts_table_name = $wpdb->prefix . 'ctctp_campaign_posts';

        if (isset($_REQUEST['campaign_id'])) {

            $campaign_id = $_REQUEST['campaign_id'];

            $q = $wpdb->prepare("SELECT * FROM $campaigns_table_name WHERE id = %d", $campaign_id);
            $campaign_item = $wpdb->get_row($q, ARRAY_A);
            if (!$campaign_item) {
                echo "Wrong campaign id";
                wp_die();
            }

            // retrieve campaign events
            if (defined('EVENT_ESPRESSO_VERSION')) {
                $events_table = new CampaignEvents_List_Table(true);
                $events_table->campaign_id = $campaign_id;
                $upcoming_events = $events_table->select_items(1, 'event_date', 'ASC');

                $campaign_events = array();
                EE_Registry::instance()->load_helper( 'Event_View' );
                foreach ($upcoming_events as $key => $event) {

                    $campaign_event = array('post_date' => '', 'event_title' => '', 'is_custom_event' => 0);

                    $ee_event = EEH_Event_View::get_event($event['id']);

                    $campaign_event['post_date'] = $ee_event->primary_datetime()->start_date_and_time();

                    $event_title = $ee_event->get( 'EVT_name' );
                    if (strlen(trim($event_title)) > 0) {
                        $event_title .= " - ";
                    }
                    $event_title .= date('gA', strtotime($ee_event->primary_datetime()->start_date_and_time()));

                    // do we have a valid event ?
                    if ($ee_event instanceof EE_Event) {
                        $tickets = $ee_event->first_datetime()->tickets();
                        $price_string = '';
                        foreach($tickets as $ticket) {
                            // add price only if it not free
                            $name = strtolower($ticket->name());
                            if (strpos($name, 'free') === false) {
                                if (strlen($price_string) > 0) {
                                    $price_string .='/';
                                }
                                $price_string = EE_Registry::instance()->CFG->currency->sign . $ticket->price() . ' at the door';
                            }
                        }

                        if (strlen($price_string) > 0) {
                            $event_title .= ' - ' . $price_string;
                        }
                    }

                    $campaign_event['event_id'] = $ee_event->get( 'EVT_ID' );
                    $campaign_event['event_title'] = $event_title;

                    $campaign_events[] = $campaign_event;
                }
            }
            // sort events by date
            usort($campaign_events, function($event1, $event2) {
                if ($event1['post_date'] == $event2['post_date']) {
                    return 0;
                }
                return (strtotime($event1['post_date']) < strtotime($event2['post_date'])) ? -1 : 1;        
            });

            $custom_events = $wpdb->get_results($wpdb->prepare("SELECT event_id, event_title, event_date FROM $campaign_events_table_name where campaign_id = %d and is_custom_event = 1", $campaign_id), ARRAY_A);

            foreach ($custom_events as $key => $event) {
                $item = array(
                        'id' => $event['event_id'],
                        'event_title' => $event['event_title'],
                        'post_date' => $event['event_date'],
                        'is_custom_event' => 1
                        );
                array_unshift($campaign_events, $item);
            }

            // retrieve campaign posts
            $campaign_posts = $wpdb->get_results($wpdb->prepare("SELECT post_id, post_title, post_content, post_date FROM $campaign_posts_table_name where campaign_id = %d  and is_reminder_post = 0 order by is_reminder_post, is_featured_post desc, post_date", $campaign_id), ARRAY_A);
            if (count($campaign_posts) > 0) {
                for($j = 0; $j < count($campaign_posts); $j ++) {
                    $campaign_posts[$j]['post_content'] = $this->pretty_post_content($campaign_posts[$j]['post_content']);
                }
            }

            // retrieve campaign reminders
            $campaign_reminders = $wpdb->get_results($wpdb->prepare("SELECT post_id, post_title, post_content, post_date FROM $campaign_posts_table_name where campaign_id = %d  and is_reminder_post = 1 and is_custom_post = 0 order by is_reminder_post, is_featured_post desc, post_date", $campaign_id), ARRAY_A);
            if (count($campaign_reminders) > 0) {
                for($j = 0; $j < count($campaign_reminders); $j ++) {
                    $campaign_reminders[$j]['post_content'] = $this->pretty_post_content($campaign_reminders[$j]['post_content']);
                }
            }

            // retrieve campaign custom post
            $campaign_custom_post = $wpdb->get_results($wpdb->prepare("SELECT post_id, post_title, post_content, post_date FROM $campaign_posts_table_name where campaign_id = %d  and is_custom_post = 1 order by is_custom_post, post_date", $campaign_id), ARRAY_A);
            if (count($campaign_custom_post) > 0) {
                for($j = 0; $j < count($campaign_custom_post); $j ++) {
                    $campaign_custom_post[$j]['post_content'] = $this->pretty_post_content($campaign_custom_post[$j]['post_content']);                    
                }
            }

            // get carleton options
            $campaign_location_query = urlencode($this->carleton_get_option("carleton_location"));
            $campaign_location = $this->carleton_get_option("carleton_location");
            $campaign_phone = $this->carleton_get_option("carleton_phone");
            $campaign_email = $this->carleton_get_option("carleton_email");

            ob_start();
            include(CTCTPOSTS_PLUGIN_DIR.'views/campaign-email-template.php');
            $campaign_content = ob_get_clean();

            if (isset($_REQUEST['is_preview']) && $_REQUEST['is_preview'] == 'true') {
                echo $campaign_content;
            }
            else {
                $campaign_item['content'] = $campaign_content;

                require_once(CTCTPOSTS_PLUGIN_DIR . 'libs/Ctct/autoload.php');
                include_once(CTCTPOSTS_PLUGIN_DIR . 'helpers/cc_includes.php');
            }
        }

        wp_die();
    }

    public function edit_post_ajax_callback() {

        global $wpdb;
        $campaign_posts_table_name = $wpdb->prefix . 'ctctp_campaign_posts';

        $default = array(
            'post_id' => '',
            'campaign_id' => '',
            'post_title' => '',
            'post_content' => ''
        );
        $item = shortcode_atts($default, $_REQUEST);

        if ($item['post_id'] != '') {
            $item = $wpdb->get_row($wpdb->prepare("SELECT post_id, post_title, post_content, post_date, campaign_id FROM $campaign_posts_table_name where post_id = %d and campaign_id = %d", $item['post_id'], $item['campaign_id']), ARRAY_A);
        }

        $editor_id   = 'editor_post_content';
        $editor_name = 'editor_post_content';
        $settings    = array (
            'tabindex'      => FALSE,
            'resize'        => TRUE,
            'textarea_name' => $editor_name,
            'tinymce' => array(
//               'force_p_newlines' => true,
                'theme_advanced_buttons1' => 'formatselect,|,bold,italic,underline,|,' .
                    'bullist,blockquote,|,justifyleft,justifycenter' .
                    ',justifyright,justifyfull,|,link,unlink,|' .
                    ',spellchecker,wp_fullscreen,wp_adv'
            )
        );

        include(CTCTPOSTS_PLUGIN_DIR.'views/edit-post.php');
        _WP_Editors::enqueue_scripts();
        print_footer_scripts();
        _WP_Editors::editor_js();

        wp_die();
    }

    public function edit_event_ajax_callback() {

        global $wpdb;
        $campaign_event_table_name = $wpdb->prefix . 'ctctp_campaign_events';
        $campaigns_table_name = $wpdb->prefix . 'ctctp_campaigns';

        $default = array(
            'event_id' => '',
            'campaign_id' => '',
            'event_title' => '',
            'event_date' => '',
            'is_custom_event' => ''
        );
        $item = shortcode_atts($default, $_REQUEST);

        if ($item['event_id'] != '') {
            $item = $wpdb->get_row($wpdb->prepare("SELECT event_id, event_title, event_date, campaign_id FROM $campaign_events_table_name where event_id = %d and campaign_id = %d", $item['event_id'], $item['campaign_id']), ARRAY_A);
        }

        include(CTCTPOSTS_PLUGIN_DIR.'views/edit-event.php');

        wp_die();
    }

    public function register_settings() {

        register_setting( 'ctct-posts-group', 'ctct_from_email' );
        register_setting( 'ctct-posts-group', 'ctct_api_key' );
        register_setting( 'ctct-posts-group', 'ctct_access_token' );
    }

    function carleton_get_option($option) {
        $options = get_option('carleton_options');

        if ($options[$option]) {
            return $options[$option];
        } else {
            return false;
        }
    }

    function pretty_post_content($post_content) {

        $post_content = wpautop(get_option('content', $post_content));
        // parse images
        $post_content = $this->pretty_images($post_content);
        // parse youtube links
        $post_content = $this->youtube_to_links($post_content);                    

        return $post_content;
    }

    function pretty_images($post_content) {

        $pattern = '/<img(.*?)>/mi';

        $post_content = preg_replace_callback(
            $pattern,
            function ($matches) {

                $img_tag = $matches[0];
                $dom = new DOMDocument();
                @$dom->loadHTML($img_tag);

                $imgs = $dom->getElementsByTagName('img');
                $img = $imgs->item(0);

                if ($img->hasAttribute('width')) {

                    $width = $img->getAttribute('width');
                    $height = $img->getAttribute('height');

                    if ($width > 393) {
                        $img->removeAttribute('height');
                        $img->setAttribute('width', '393');

                        $img_content = '<img ';
                        if ($img->hasAttributes()) {
                            foreach ($img->attributes as $attr) {
                                $name = $attr->nodeName;
                                $value = $attr->nodeValue;
                                $img_content .= $name .'="' . $value .'" ';
                            }
                        }
                        $img_content .= ' style="width:393px;height:auto;border-width:0;border-style:none;margin-top:16px;margin-bottom:16px;"/>';
                        return $img_content;
                    }
                }
                return $img_tag;
            },
            $post_content
        );

        return $post_content;
    }

    private function youtube_to_links($post_content) {

        $post_content = $this->youtube_to_links_by_tag($post_content, 'a', 'href');
        $post_content = $this->youtube_to_links_by_tag($post_content, 'iframe', 'src');
        $post_content = $this->youtube_to_links_by_tag($post_content, 'p', '');

        return $post_content;
    }

    private function youtube_to_links_by_tag($post_content, $tag, $attr) {

        $pattern_link = "/<".$tag."([^>]+)>(.+?)<\/".$tag.">/";
        if ($tag == 'p') {
            $pattern_link = "/<".$tag."([^>]*)>(.+?)<\/".$tag.">/";
        }

        $obj = $this;
        $post_content = preg_replace_callback(
            $pattern_link,
            function ($matches) use ($tag, $attr, $obj) {

                $link_tag = $matches[0];
                $dom = new DOMDocument();
                @$dom->loadHTML($link_tag);

                $links = $dom->getElementsByTagName($tag);
                $link = $links->item(0);

                if ($tag == 'p' || $link->hasAttribute($attr)) {

                    $src = ($tag == 'p') ? $link->textContent : $link->getAttribute($attr);

                    $pattern_youtube = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/";
                    $matches = array();
                    if (preg_match($pattern_youtube, $src, $matches)) {

                        $youtube_id = $matches[1];
                        $image_url = $obj->add_video_play_icon($youtube_id);

                        $link_content = '<p style="margin-top:10px;margin-bottom:10px;">';
                        $link_content .= '<a class="imgCaptionAnchor" track="on" shape="rect" target="_blank" ';
                        $link_content .= ' href="' . $src . '" atl="' . $src . '">';
                        $link_content .= '<img src="' . $image_url . '"/>';
                        $link_content .= '</a>';
                        $link_content .= '</p>';

                        return $link_content;
                    }
                }
                return $link_tag;
            },
            $post_content
        );

        return $post_content;
    }

    function add_video_play_icon($youtube_id) {

        $image_url = 'http://img.youtube.com/vi/' . $youtube_id . '/mqdefault.jpg';

        $check_url = "http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=".$youtube_id."&format=json";
        $file_headers = @get_headers($check_url);
        if($file_headers[0] == 'HTTP/1.0 404 Not Found' || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return $image_url;
        }

        $top_image_src = CTCTPOSTS_PLUGIN_DIR . "/images/video-play-48.png";
        $upload_dir = wp_upload_dir();

        $basedir = $upload_dir['basedir'] . "/constant-contact-posts";
        if (!file_exists($basedir)) {
            mkdir($basedir);
        }
        $filename = 'youtube_' . $youtube_id . '.png';

        $base_image = imagecreatefromjpeg($image_url);
        $top_image = imagecreatefrompng($top_image_src);

        $merged_image = $basedir . '/' . $filename;
        $merged_image_url = $upload_dir['baseurl'] . "/constant-contact-posts/" . $filename;

        list($width, $height) = getimagesize($top_image_src);
        list($newwidth, $newheight) = getimagesize($image_url);

        imagesavealpha($top_image, true);
        imagealphablending($top_image, true);

        imagecopy($base_image, $top_image, ($newwidth - $width) / 2, ($newheight - $height) / 2, 0, 0, $width, $height);
        imagepng($base_image, $merged_image);

        return $merged_image_url;
    }

    function set_top_event_callback() {

        global $wpdb;
        $campaign_posts_table_name = $wpdb->prefix . 'ctctp_campaign_posts';

        $campaign_id = $_REQUEST['campaign_id'];
        $post_id = $_REQUEST['post_id'];

        $wpdb->update($campaign_posts_table_name,
            array('is_featured_post' => 0),
            array('campaign_id' => $campaign_id)
        );

        $wpdb->update($campaign_posts_table_name,
            array('is_featured_post' => 1),
            array('campaign_id' => $campaign_id, 'post_id' => $post_id)
        );
    }

    function set_reminder_event_ajax_callback() {

        global $wpdb;
        $campaign_posts_table_name = $wpdb->prefix . 'ctctp_campaign_posts';

        $campaign_id = $_REQUEST['campaign_id'];
        $post_id = $_REQUEST['post_id'];
        $is_reminder_post = $_REQUEST['is_reminder_post'];

        $wpdb->update($campaign_posts_table_name,
            array('is_reminder_post' => $is_reminder_post),
            array('campaign_id' => $campaign_id, 'post_id' => $post_id)
        );
    }
}
?>