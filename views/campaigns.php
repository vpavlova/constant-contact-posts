<div class="wrap">

    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2><?php _e('Campaigns', 'ctctp')?> <a class="add-new-h2"
                                 href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page='. $menu_slug . '/edit-campaign');?>"><?php _e('Add new', 'ctctp')?></a>
    </h2>
    <?php echo $message; ?>

    <form id="campaigns-table" method="GET">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
        <?php $table->display() ?>
    </form>

</div>