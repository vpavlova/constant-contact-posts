<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta charset="utf-8">
    <title>Carleton</title>
    <meta name="viewport" content="width=device-width">
    <style type="text/css">
        html,
        body {
            background-color: #ededed;
        }

        body {
            font-family: Arial,sans-serif;
        }

        p {
            margin-top: 0;
        }

        .post img {
            max-width: 100%;
            height: auto!important;
        }

        .post thead th a:hover,
        .event thead th a:hover {
            color: #C65E33!important;
        }
    </style>
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
    <table class="body" cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" bgcolor="#ededed" style="font-family:Arial,sans-serif;">
        <tbody>
            <tr>
                <td class="body-td" style="text-align:center;padding-top:20px;padding-bottom:5px;padding-left:20px;padding-right:20px;vertical-align:top;" align="center">
                    <table class="container" style="margin-left:auto;margin-right:auto;width:700px;text-align:center;border-color:#dbdbdb;border-width:1px;border-style:solid;" cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr class="header">
                                <td bgcolor="#2e2e2e" style="padding-top:27px;padding-bottom:25px;color:white;font-family:Arial,sans-serif;"><a href="#"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/carleton-logo.png" alt="The Carleton Music Bar &amp; Grill" width="237" height="45" border="0"></a></td>
                            </tr> <!-- .header -->
                            <tr class="nav">
                                <td bgcolor="#c75d33" style="color:white;text-align:center;padding-top:21px;padding-bottom:21px;font-family:Arial,sans-serif;">
                                    <table class="menu" style="width:625px;margin-left:auto;margin-right:auto;text-align:center;padding-top:0;" cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="stack" valign="middle" style="color:white;text-transform:uppercase;font-size:13px;line-height:15px;border-right-width:1px;border-right-color:#dd9e85;border-right-style:solid;white-space:nowrap;font-family:Arial,sans-serif;"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/icon-location.png" valign="top" alt="" width="14" height="14"> <a target="_blank" href="http://maps.apple.com/?q=<?php echo $campaign_location_query; ?>" style="color:#ffffff;text-decoration:none;vertical-align:top"><?php echo $campaign_location; ?></a></td>
                                                <td class="stack" valign="middle" style="color:white;text-transform:uppercase;font-size:13px;line-height:15px;border-right-width:1px;border-right-color:#dd9e85;border-right-style:solid;white-space:nowrap;font-family:Arial,sans-serif;"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/icon-phone.png" valign="top" alt="" width="14" height="14"> <a target="_blank" value="+79024226335" href="tel:902-422-6335" style="color:#ffffff;text-decoration:none;vertical-align:top"><?php echo $campaign_phone; ?></a></td>
                                                <td class="stack" valign="middle"><a style="color:white;font-size:13px;line-height:15px;text-decoration:none;white-space:nowrap;font-family:Arial,sans-serif;" href="mailto:info@thecarleton.ca"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/icon-email.png" valign="top" border="0" alt="" width="14" height="14"> <?php echo $campaign_email; ?></a></td>
                                            </tr>
                                        </tbody>
                                    </table> <!-- .menu -->
                                </td>
                            </tr> <!-- .nav -->
                            <tr class="content-tr">
                                <td bgcolor="#ffffff">
                                    <table class="content" width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="aside stack" valign="top" width="218" style="padding-top:27px;padding-right:0;padding-bottom:29px;padding-left:29px;font-family:Arial,sans-serif;">
                                                <?php if (defined('EVENT_ESPRESSO_VERSION')) : ?>
                                                    <table class="upcoming" style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-transform:uppercase;color:#c65e33;font-size:18px;line-height:22px;font-weight:700;text-align:left;padding-bottom:0;font-family:Arial,sans-serif;">Upcoming shows</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            $is_first = true;
                                                            foreach($campaign_events as $key => $event ) : ?>
                                                            <tr>
                                                                <?php
                                                                    $td_style = "padding-top:12px;padding-bottom:12px;";
                                                                    $uppercased_date = "";
                                                                    if ($is_first) {
                                                                        $is_first = false;
                                                                    }
                                                                    else {
                                                                        $td_style .= "border-top-style:solid;border-top-width:1px;border-top-color:#e8e8e8;";
                                                                    }
                                                                ?>
                                                                <td style="<?php echo $td_style; ?>">
                                                                    <table class="event" style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="<?php echo $uppercased_date; ?>text-align:left;font-size:14px;font-weight:700;color:#484848;padding-top:0;padding-bottom:5px;font-family:Arial,sans-serif;"><a href="<?php echo get_permalink($event['event_id']) ?>" style="text-align:left;font-size:14px;font-weight:700;color:#484848;padding-top:0;padding-bottom:5px;font-family:Arial,sans-serif;text-decoration:none;"><?php if ($event['is_custom_event']) { echo $event['post_date']; } else  { echo date('l, F jS', strtotime($event['post_date'])); } ?></a></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="text-align:left;font-size:12px;color:#868686;font-family:Arial,sans-serif;"><?php echo $event['event_title']; ?> </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table> <!-- .event -->
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                            <tr>
                                                                <td style="padding-top:12px;padding-bottom:12px;border-top-style:solid;border-top-width:1px;border-top-color:#e8e8e8;font-size:13px;line-height:18px;color:#C65E33;text-align:left;font-family:Arial,sans-serif;">All ticket purchases subject to $2.50 booking fee (we don't make money on this folks)</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                <?php else : ?>
                                                    &nbsp;
                                                <?php endif; ?>
                                                </td>
                                                <td class="main stack" valign="top" style="padding-top:27px;padding-right:29px;padding-bottom:29px;padding-left:29px;">
                                                    <table class="posts" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>
                                                        <?php foreach($campaign_posts as $key => $post ) : ?>

                                                            <tr>
                                                                <td>
                                                                    <table class="post" style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="text-align:left;text-transform:uppercase;font-size:30px;line-height:30px;font-weight:700;color:#484848;padding-bottom:9px;font-family:Arial,sans-serif;"><a href="<?php echo get_permalink($post['post_id']) ?>" style="text-align:left;text-transform:uppercase;text-decoration:none;font-size:30px;line-height:30px;font-weight:700;color:#484848;padding-bottom:9px;font-family:Arial,sans-serif;"><?php echo $post['post_title'] ?></a></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left;padding-bottom:20px;"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/line.png" alt="" width="54" height="2" border="0"></td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="text-align:left;font-size:13px;line-height:18px;color:#868686;padding-bottom:50px;font-family:Arial,sans-serif;">
                                                                                    <h3 style="color:#484848;text-transform:uppercase;font-weight:400;margin:0;font-size:14px;"><?php echo date('l, F jS', strtotime($post['post_date'])); ?></h3>
                                                                                    <?php
                                                                                        // $pattern = '/<img(.*?)>/mi';
                                                                                        // $post_content = preg_replace($pattern, "<img$1 style='max-width:100%'>", $post['post_content']);
                                                                                    ?>
                                                                                    <?php echo $post['post_content']; ?>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table> <!-- .post -->
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                <?php if (isset($campaign_reminders) && count($campaign_reminders) > 0) : ?>

                                                    <table class="posts" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <thead>
                                                            <tr><th style="border-top-style:solid;border-top-width:1px;border-top-color:#e8e8e8;text-transform:uppercase;color:#c65e33;font-size:18px;line-height:22px;font-weight:700;text-align:left;padding-top:12px;padding-bottom:24px;font-family:Arial,sans-serif;">Reminders</th></tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach($campaign_reminders as $key => $post ) : ?>

                                                            <tr>
                                                                <td>
                                                                    <table class="post" style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="text-align:left;text-transform:uppercase;font-size:30px;line-height:30px;font-weight:700;color:#484848;padding-bottom:9px;font-family:Arial,sans-serif;"><a href="<?php echo get_permalink($post['post_id']) ?>" style="text-align:left;text-transform:uppercase;text-decoration:none;font-size:30px;line-height:30px;font-weight:700;color:#484848;padding-bottom:9px;font-family:Arial,sans-serif;"><?php echo $post['post_title'] ?></a></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left;padding-bottom:20px;"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/line.png" alt="" width="54" height="2" border="0"></td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="text-align:left;font-size:13px;line-height:18px;color:#868686;padding-bottom:50px;font-family:Arial,sans-serif;">
                                                                                    <h3 style="color:#484848;text-transform:uppercase;font-weight:400;margin:0;font-size:14px;"><?php echo date('l, F jS', strtotime($post['post_date'])); ?></h3>
                                                                                    <?php
                                                                                        // $pattern = '/<img(.*?)>/mi';
                                                                                        // $post_content = preg_replace($pattern, "<img$1 style='max-width:100%'>", $post['post_content']);
                                                                                    ?>
                                                                                    <?php echo $post['post_content']; ?>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table> <!-- .post -->
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                <? endif; ?>
                                                <?php if (isset($campaign_custom_post) && count($campaign_custom_post) > 0) : ?>

                                                    <table class="posts" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>
                                                        <?php foreach($campaign_custom_post as $key => $post ) : ?>

                                                            <tr>
                                                                <td>
                                                                    <table class="post" style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style="text-align:left;text-transform:uppercase;font-size:30px;line-height:30px;font-weight:700;color:#484848;padding-bottom:9px;font-family:Arial,sans-serif;"><a href="<?php echo get_permalink($post['post_id']) ?>" style="text-align:left;text-transform:uppercase;text-decoration:none;font-size:30px;line-height:30px;font-weight:700;color:#484848;padding-bottom:9px;font-family:Arial,sans-serif;"><?php echo $post['post_title'] ?></a></th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="text-align:left;padding-bottom:20px;"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/line.png" alt="" width="54" height="2" border="0"></td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="text-align:left;font-size:13px;line-height:18px;color:#868686;padding-bottom:50px;font-family:Arial,sans-serif;">
                                                                                    <h3 style="color:#484848;text-transform:uppercase;font-weight:400;margin:0;font-size:14px;"><?php echo date('l, F jS', strtotime($post['post_date'])); ?></h3>
                                                                                    <?php
                                                                                        // $pattern = '/<img(.*?)>/mi';
                                                                                        // $post_content = preg_replace($pattern, "<img$1 style='max-width:100%'>", $post['post_content']);
                                                                                    ?>
                                                                                    <?php echo $post['post_content']; ?>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table> <!-- .post -->
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                <? endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="footer" colspan="2" style="padding-top:29px;padding-right:29px;padding-bottom:23px;padding-left:29px;">
                                                    <table width="100%" style="border-top-style:solid;border-top-width:1px;border-top-color:#e5e5e5;">
                                                        <tbody>
                                                            <tr>
                                                                <td class="footer-list stack" style="padding-top:29px;padding-bottom:6px;width:52%;text-align:left;">&nbsp;</td>
                                                                <td class="footer-fb stack" style="padding-top:29px;padding-bottom:6px;text-align:center;width:16%;"><a href="https://www.facebook.com/pages/The-Carleton-Music-Bar-Grill/215472491820022"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/icon-facebook.png" alt="Follow us on Facebook" border="0"></a></td>
                                                                <td class="footer-tw stack" style="padding-top:29px;padding-bottom:6px;text-align:center;width:16%;"><a href="https://twitter.com/carletonhalifax"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/icon-twitter.png" alt="Follow us on Twitter" border="0"></a></td>
                                                                <td class="footer-ea stack" style="padding-top:29px;padding-bottom:6px;text-align:right;width:16%;"><a href="http://www.everageconsulting.com"><img src="http://www.thecarleton.ca/wp-content/themes/carleton/images/email/icon-everage.png" alt="Done by EverAge" border="0"></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td> <!-- .footer -->
                                            </tr>
                                        </tbody>
                                    </table> <!-- .main -->
                                </td>
                            </tr> <!-- .content -->
                        </tbody>
                    </table> <!-- .container -->
                    <table class="bottom" cellpadding="0" cellspacing="0" border="0" style="margin-left:auto;margin-right:auto;width:700px;text-align:center;">
                        <tbody>
                            <tr>
                                <td style="color:#b9b9b9;padding-top:34px;padding-right:29px;padding-bottom:50px;padding-left:29px;font-size:13px;font-family:Arial,sans-serif;">The Carleton Music Bar &amp; Grill  /  1685 Argyle St  /  Halifax  /  NS  /  B3J 2B5  /  Canada</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table> <!-- .body -->
</body>
</html>