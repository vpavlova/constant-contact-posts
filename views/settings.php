<div class="wrap">
<h2><?php echo __('Constant Contacts Settings', 'ctctp') ?></h2>

<form method="post" action="options.php">
    <?php settings_fields( 'ctct-posts-group' ); ?>
    <?php do_settings_sections( 'ctct-posts-group' ); ?>
    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php echo __('Constant Contacts From email', 'ctctp') ?></th>
        <td><input class="settings" type="text" name="ctct_from_email" value="<?php echo esc_attr( get_option('ctct_from_email') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php echo __('Constant Contacts api key', 'ctctp') ?></th>
        <td><input class="settings" type="text" name="ctct_api_key" value="<?php echo esc_attr( get_option('ctct_api_key') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row"><?php echo __('Constant Contacts token', 'ctctp') ?></th>
        <td><input class="settings" type="text" name="ctct_access_token" value="<?php echo esc_attr( get_option('ctct_access_token') ); ?>" /></td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
