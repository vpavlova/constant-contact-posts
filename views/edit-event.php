<input type="hidden" name="campaign_event_id" value="<?php echo stripslashes($item['event_id']) ?>"/>
<table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
    <tbody>
    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="event_title"><?php _e('Event title', 'ctctp')?></label>
        </th>
        <td>
            <input id="event_title" name="event_title" type="text" style="width: 95%" value="<?php echo $item['event_title']?>"
                   size="100" class="code" placeholder="<?php _e('Event title', 'ctctp')?>" required>
        </td>
    </tr>
	<tr class="form-field">
        <th valign="top" scope="row">
            <label for="event_date"><?php _e('Event date', 'ctctp')?></label>
        </th>
        <td>
            <input id="event_date" name="event_date" type="text" style="width: 95%" value="<?php echo $item['event_date']?>"
                   size="100" class="code" placeholder="<?php _e('Event date', 'ctctp')?>" required>
        </td>
	</tr>
    </tbody>
</table>