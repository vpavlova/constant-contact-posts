<div class="wrap ctctp-block">
    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2><?php _e('Add/edit campaign', 'ctctp')?> <a class="add-new-h2"
                                href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=' . $menu_slug . '/campaigns');?>"><?php _e('back to list', 'ctctp')?></a>
    </h2>

    <?php if (!empty($notice)): ?>
    <div id="notice" class="error"><p><?php echo $notice ?></p></div>
    <?php endif;?>
    <?php if (!empty($message)): ?>
    <div id="message" class="updated"><p><?php echo $message ?></p></div>
    <?php endif;?>

    <form id="form" method="POST">
        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce($nonce)?>"/>
        <input type="hidden" name="id" id="campaign_id" value="<?php echo $item['id'] ?>" />

        <div class="metabox-holder" id="poststuff">
            <div id="post-body">
                <div id="post-body-content">

					<table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
					    <tbody>
					    <tr class="form-field">
					        <th valign="top" scope="row">
					            <label for="name"><?php _e('Name', 'ctctp')?></label>
					        </th>
					        <td>
					            <input id="name" name="name" type="text" style="width: 95%" value="<?php echo esc_attr($item['name'])?>"
					                   size="50" class="code" placeholder="<?php _e('Campaign name', 'ctctp')?>" required>
					        </td>
					    </tr>
					    </tbody>
					</table>
                    <input type="submit" value="<?php if ($item['id'] == '') _e('Save', 'ctctp'); else _e('Update name', 'ctctp'); ?>" id="submit" class="button-primary" name="submit">
                </div>
            </div>
        </div>

<?php if(isset($item['id']) && $item['id'] != 0) { ?>

<?php 
    $item['is_filled_posts'] = (isset($posts_table) && count($posts_table) > 0);
?>
<?php if (defined('EVENT_ESPRESSO_VERSION')) : ?>
	<hr class="events" style="clear:both;margin:1em 0 1.5em;">
	<div id="events">
	    <h2><?php _e('Events', 'ctctp')?></h2>
        <?php $events_table->display() ?>
	</div>
<?php endif; ?>

	<hr class="posts" style="clear:both;margin:2em 0 1.5em;">
<?php	if (!(bool)$item['is_filled_posts']): ?>
            <input type="button" value="<?php _e('Select events', 'ctctp')?>" id="select-posts" class="button-primary" name="select-posts">
<?php 	else: ?>
			<div id="posts">
			    <h2><?php _e('New Events', 'ctctp')?><a class="add-new-h2" href="javascript:void(0)" id="select-posts"><?php _e('Add events', 'ctctp')?></a></h2>
		        <?php $posts_table->display() ?>
			</div>
<?php	endif; ?>

	<hr class="posts" style="clear:both;margin:2em 0 1.5em;">
<?php	if (!(bool)$item['is_filled_reminders']): ?>
            <input type="button" value="<?php _e('Select reminders', 'ctctp')?>" id="select-reminders" class="button-primary" name="select-reminders">
<?php 	else: ?>
			<div id="reminders">
			    <h2><?php _e('Reminders', 'ctctp')?><a class="add-new-h2" href="javascript:void(0)" id="select-reminders"><?php _e('Add reminders', 'ctctp')?></a></h2>
		        <?php $reminders_table->display() ?>
			</div>
<?php	endif; ?>

	<hr class="posts" style="clear:both;margin:2em 0 1.5em;">
	<div id="custom_post">
<?php	if (!$is_custom_post) : ?>
		    <a class="add-new-h2" href="javascript:void(0)" id="add-custom-post"><?php _e('Add custom post', 'ctctp') ?></a>
<?php 	else: ?>
			    <h2><?php _e('Custom post', 'ctctp')?></h2>
		        <?php $custom_post_table->display() ?>
<?php	endif; ?>
	</div>

	<hr class="posts" style="clear:both;margin:2em 0 1.5em;">
<?php if ((defined('EVENT_ESPRESSO_VERSION')) && ((bool)$item['is_filled_posts'] || (bool)$item['is_filled_reminders'])): ?>
	<br/>
	<div>
		<input type="button" value="<?php _e('Preview campaign', 'ctctp')?>" id="preview-campaign" class="button-secondary" name="preview-campaign">
		<input type="button" value="<?php _e('Send campaign', 'ctctp')?>" id="send-campaign" class="button-primary" name="send-campaign"><span class="spinner" id="send-campaign-spinner"></span>
		<div id="campaign-status" class="hidden"></div>
	</div>
<?php endif; ?>

<?php } ?>
    </form>

</div>

	<div id="find-posts" class="find-box" style="display:none;">
	    <form id="form-posts" method="POST">
	        <input type="hidden" name="id" id="_id" value="<?php echo $item['id'] ?>" />
	        <input type="hidden" name="is_reminder_post" id="is_reminder_post" value="0" />

			<div id="find-posts-head" class="find-box-head hidden"><?php _e( 'Select events', 'ctctp' ); ?></div>
			<div id="find-reminders-head" class="find-box-head hidden"><?php _e( 'Select reminders', 'ctctp' ); ?></div>
			<div id="edit-post-head" class="find-box-head hidden"><?php _e( 'Edit event', 'ctctp' ); ?></div>
			<div id="create-campaign-post-head" class="find-box-head hidden"><?php _e( 'Create campaign post', 'ctctp' ); ?></div>

			<div class="find-box-inside">
				<span class="spinner"></span>
		        <?php wp_nonce_field( 'find-posts', '_ajax_nonce', false ); ?>
				<div id="select-posts-response"></div>
			</div>
			<div class="find-box-buttons">
				<input id="find-posts-close" type="button" class="button" value="<?php esc_attr_e( 'Close' ); ?>" style="float:left;position:relative;width:auto;height:auto"/>
				<?php submit_button( __( 'Select' ), 'find-box-button button-primary alignright hidden', 'find-posts-submit', false );  ?>
				<?php submit_button( __( 'Save' ), 'find-box-button button-primary alignright hidden', 'edit-posts-submit', false );  ?>
			</div>
		</form>
	</div>