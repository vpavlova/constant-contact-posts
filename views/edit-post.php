<input type="hidden" name="campaign_post_id" value="<?php echo stripslashes($item['post_id']) ?>"/>
<table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
    <tbody>
    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="post_title"><?php _e('Event title', 'ctctp')?></label>
        </th>
        <td>
            <input id="post_title" name="post_title" type="text" style="width: 95%" value="<?php echo $item['post_title']?>"
                   size="100" class="code" placeholder="<?php _e('Campaign event title', 'ctctp')?>" required>
        </td>
    </tr>
	<tr class="form-field">
	    <td colspan="2">
	        <?php
	        	wp_editor($item['post_content'], $editor_id, $settings);
	        ?>
	    </td>
	</tr>
    </tbody>
</table>
