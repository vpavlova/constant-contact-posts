<div class="campaign-posts">

    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
    <h2><?php _e('Posts', 'ctctp')?></h2>

    <form id="campaign-posts-table" method="GET">
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
	    <?php $table->display() ?>
    </form>

</div>