var bawmrpFindPosts;
jQuery(document).ready(function($) {
	bawmrpFindPosts = {
		tag: 'find-posts',
		open: function( af_name, af_val ) {
			var overlay = $( '.ui-find-overlay' );

			if ( overlay.length === 0 ) {
				$( 'body' ).append( '<div class="ui-find-overlay"></div>' );
				bawmrpFindPosts.overlay();
			}

			overlay.show();

			if ( af_name && af_val ) {
				$( '#affected' ).attr( 'name', af_name ).val( af_val );
			}

			$( '#' + this.tag ).show();

			$('#' + this.tag + '-input').focus().keyup( function( event ){
				if ( event.which == 27 ) {
					bawmrpFindPosts.close();
				} // close on Escape
			});

			// Pull some results up by default
			bawmrpFindPosts.send();

			return false;
		},

		close : function() {
			$('#' + bawmrpFindPosts.tag + '-response').html('');
			$('#' + bawmrpFindPosts.tag).hide();
			$('.ui-find-overlay').hide();
			// $('#find-posts').draggable('destroy').hide();
		},

		overlay: function() {
			$( '.ui-find-overlay' ).on( 'click', function () {
				bawmrpFindPosts.close();
			});
		},

		send: function() {
			var $pt = '';
			$('input[name="' + bawmrpFindPosts.tag + '-what[]"]:checked').each(function(){
				$pt += $( this ).val() + ',';
			});
			var post = {
					ps: $( '#' + bawmrpFindPosts.tag + '-input' ).val(),
					action: 'bawmrp_ajax_find_posts',
					_ajax_nonce: $('#_ajax_nonce').val(),
					post_type: $pt
				},
				spinner = $( '.find-box-search .spinner' );
			spinner.show();
			$.ajax( ajaxurl, {
				type: 'POST',
				data: post,
				dataType: 'json'
			}).always( function() {
				spinner.hide();
			}).done( function( x ) {
				if ( ! x ) {
					$( '#' + bawmrpFindPosts.tag + '-response' ).text( x.responseText );
				}
				$( '#' + bawmrpFindPosts.tag + '-response' ).html( x.data );
			}).fail( function( x ) {
				$( '#' + bawmrpFindPosts.tag + '-response' ).text( x.responseText );
			});
		}
	};

	$( '#' + bawmrpFindPosts.tag + '-search' ).click( bawmrpFindPosts.send );
	$( '#' + bawmrpFindPosts.tag + '-close' ).click( bawmrpFindPosts.close );

}); 
