var wp_table_list;
jQuery(document).ready(function($) {
 
    wp_table_list = {
     
        action: '',
        table_el: '',

        create: function(list, action, table_el) {

            list.table_el = table_el + ' ';
            list.search = '';
            list.init(list, action);
        },

        /**
         * Register our triggers
         * 
         * We want to capture clicks on specific links, but also value change in
         * the pagination input field. The links contain all the information we
         * need concerning the wanted page number or ordering, so we'll just
         * parse the URL to extract these variables.
         * 
         * The page number input is trickier: it has no URL so we have to find a
         * way around. We'll use the hidden inputs added in TT_Example_List_Table::display()
         * to recover the ordering variables, and the default paged input added
         * automatically by WordPress.
         */
        init: function(list, action) {
     
            if (typeof(action) != 'undefined') {
                list.action = action;
            }

            // This will have its utility when dealing with the page number input
            var timer;
            var delay = 500;
     
            // Pagination links, sortable link
            $(list.table_el).on('click', '.tablenav-pages a, .manage-column.sortable a, .manage-column.sorted a', function(e) {
                // We don't want to actually follow these links
                e.preventDefault();
                // Simple way: use the URL to extract our needed variables
                var query = this.search.substring( 1 );
                 
                var data = {
                    paged: list.__query( query, 'paged' ) || parseInt( $('input[name=paged]').val() ) || '1',
                    order: list.__query( query, 'order' ) || $('input[name=order]').val() || 'asc',
                    orderby: list.__query( query, 'orderby' ) || $('input[name=orderby]').val() || 'title'
                };
                
                var items_added = [];
                $('.items-available-added input.item-added').each(function() {
                    items_added.push($(this).val());
                });
                data['items_added'] = items_added;
                data['id'] = $('.ctctp-block input#campaign_id').val();

                list.update( data, list );
            });
     
            // Page number input
            $('input[name=paged]').on('keyup', function(e) {
     
                // If user hit enter, we don't want to submit the form
                // We don't preventDefault() for all keys because it would
                // also prevent to get the page number!
                if ( 13 == e.which )
                    e.preventDefault();
     
                // This time we fetch the variables in inputs
                var data = {
                    paged: parseInt( $('input[name=paged]').val() ) || '1',
                    order: $('input[name=order]').val() || 'asc',
                    orderby: $('input[name=orderby]').val() || 'title'
                };
     
                // Now the timer comes to use: we wait half a second after
                // the user stopped typing to actually send the call. If
                // we don't, the keyup event will trigger instantly and
                // thus may cause duplicate calls before sending the intended
                // value
                window.clearTimeout( timer );
                timer = window.setTimeout(function() {
                    list.update( data, list );
                }, delay);
            });
        },
     
        /** AJAX call
         * 
         * Send the call and replace table parts with updated version!
         * 
         * @param    object    data The data to pass through AJAX
         */
        update: function( data, list ) {
            $.ajax({
                // /wp-admin/admin-ajax.php
                url: ajaxurl,
                // Add action and nonce to our collected data
                data: $.extend(
                    {
                        _ajax_custom_list_nonce: $('#_ajax_custom_list_nonce').val(),
                        action: list.action,
                    },
                    data
                ),
                // Handle the successful result
                success: function( response ) {
     
                    // WP_List_Table::ajax_response() returns json
                    var response = $.parseJSON( response );
     
                    // Add the requested rows
                    if ( response.rows.length )
                        $(list.table_el + '#the-list').html( response.rows );
                    // Update column headers for sorting
                    if ( response.column_headers.length )
                        $(list.table_el + 'thead tr, #find-posts tfoot tr').html( response.column_headers );
                    // Update pagination for navigation
                    if ( typeof(response.pagination) != 'undefined' && typeof(response.pagination.top) != 'undefined' && response.pagination.top.length )
                         $(list.table_el + ' .tablenav.top .tablenav-pages').html( $(response.pagination.top).html() );
                    // if ( response.pagination.bottom.length )
                    //     $(list.table_el + ' .tablenav.bottom .tablenav-pages').html( $(response.pagination.bottom).html() );
     
                    // Init back our event handlers
                    //list.init(list);
                }
            });
        },
     
        /**
         * Filter the URL Query to extract variables
         * 
         * @see http://css-tricks.com/snippets/javascript/get-url-variables/
         * 
         * @param    string    query The URL query part containing the variables
         * @param    string    variable Name of the variable we want to get
         * 
         * @return   string|boolean The variable value if available, false else.
         */
        __query: function( query, variable ) {
     
            var vars = query.split("&");
            for ( var i = 0; i <vars.length; i++ ) {
                var pair = vars[ i ].split("=");
                if ( pair[0] == variable )
                    return pair[1];
            }
            return false;
        },
    };
 
}); 