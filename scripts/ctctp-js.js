jQuery(document).ready(function($) {

	$('.ctctp-block').on( 'click', '#select-posts,#select-reminders', function(e) {

		e.preventDefault();

		$('.find-box-head').hide();
		if (e.target.id == 'select-posts') {
			$('#find-posts-head').show();
			$('#is_reminder_post').val(0);
		}
		else {
			$('#find-reminders-head').show();
			$('#is_reminder_post').val(1);
		}

		$('.find-box-button').hide();
		$('#find-posts-submit').show();

		bawmrpFindPosts.open();
		
		$('#select-posts-response').html('');
		spinner = $( '.find-box-inside .spinner' );
		spinner.show();

		var data = {
				'action': 'ctctp_select_posts',
				'id': $('.ctctp-block input#campaign_id').val()
			};

		$.post(
			ajaxurl,
			data,
			function( response ) {
				spinner.hide();
				$('#select-posts-response').html(response);
				wp_table_list.create(wp_table_list, 'ctctp_select_posts_ajax', '#find-posts');
				$('#select-posts-response').fadeIn(function() {
					resizeTable();
				});
			}
		);
	});

	$('.ctctp-block').on( 'click', '#select-events', function(e) {

		e.preventDefault();

		$('.find-box-head').hide();
		$('#find-events-head').show();

		$('.find-box-button').hide();
		$('#find-posts-submit').show();

		bawmrpFindPosts.open();
		
		$('#select-posts-response').html('');
		spinner = $( '.find-box-inside .spinner' );
		spinner.show();

		var data = {
				'action': 'ctctp_select_events',
				'id': $('.ctctp-block input#campaign_id').val()
			};

		$.post(
			ajaxurl,
			data,
			function( response ) {
				spinner.hide();
				$('#select-posts-response').html(response);
				wp_table_list.create(wp_table_list, 'ctctp_select_events_ajax', '#find-posts');
				$('#select-posts-response').fadeIn(function() {
					resizeTable();
				});
			}
		);
	});

	$(document).on( 'click', '.item-available', function(e) {

		if($(this).prop('checked')) {
			var post_id = $(this).val();
			var item_name = ($(this).hasClass('post-available')) ? 'posts_added' : 'events_added';
			$('.items-available-added').append('<input type="hidden" class="item-added" id="post-added-'+post_id+'" name="'+item_name+'[]" value="'+post_id+'"/>');	
		}
		// else find and remove item from list
		else {
			var post_id = $(this).val();
			$('.items-available-added').find('input#post-added-'+post_id).remove();	
		}
	});

	$('.ctctp-block').on( 'click', '#preview-campaign,#send-campaign', function(e) {

		e.preventDefault();

		if ($('.ctctp-block #preview-campaign').hasClass('disabled') || $('.ctctp-block #send-campaign').hasClass('disabled')) {
			return;
		}

		$(e.target).addClass('disabled');
		$('.ctctp-block #campaign-status').html('').hide();

		spinner = $( '#send-campaign-spinner' );
		spinner.css('visibility', 'visible');

		var data = {
				'action': 'ctctp_send_campaign',
				'campaign_id': $('.ctctp-block input#campaign_id').val(),
				'is_preview': $(e.target).attr('id') == 'preview-campaign'
			};

		$.post(
			ajaxurl,
			data,
			function( response ) {
				spinner.css('visibility', 'hidden');
				$('.ctctp-block #campaign-status').html(response).show();

				$(e.target).removeClass('disabled');
			}
		);
	});

	$('#posts,#reminders,#custom_post').on( 'click', '.edit-campaign-post, #add-custom-post', function(e) {

		e.preventDefault();

		$('.find-box-head').hide();
		if (e.target.id == 'add-custom-post') {
			$('#create-campaign-post-head').show();
			$('#is_reminder_post').val(1);
		}
		else {
			$('#edit-post-head').show();
		}		

		$('.find-box-button').hide();
		$('#edit-posts-submit').show();

		bawmrpFindPosts.open();
		
		$('#select-posts-response').html('');
		spinner = $( '.find-box-inside .spinner' );
		spinner.show();

		var data = {
				'action': 'ctctp_edit_post_ajax',
				'post_id': $(this).data('post-id'),
				'campaign_id': $(this).data('campaign-id')
			};

		$.post(
			ajaxurl,
			data,
			function( response ) {
				spinner.hide();

				$('#select-posts-response').html(response);
				tinymce.init({selector:'posteditor'});

				$('#select-posts-response').fadeIn();
			}
		);
	});

	$('#events').on( 'click', '#add-custom-event', function(e) {

		e.preventDefault();

		$('.find-box-head').hide();
		$('#add-event-head').show();

		$('.find-box-button').hide();
		$('#edit-events-submit').show();

		bawmrpFindPosts.open();
		
		$('#select-posts-response').html('');
		spinner = $( '.find-box-inside .spinner' );
		spinner.show();

		var data = {
				'action': 'ctctp_edit_event_ajax',
				'event_id': $(this).data('event-id'),
				'campaign_id': $(this).data('campaign-id')
			};

		$.post(
			ajaxurl,
			data,
			function( response ) {
				spinner.hide();

				$('#select-posts-response').html(response);
				tinymce.init({selector:'posteditor'});

				$('#select-posts-response').fadeIn();
			}
		);
	});

	$('.ctctp-block').on( 'click', '#edit-posts-submit', function(e) {

		e.preventDefault();

		if ($('.ctctp-block #preview-campaign').hasClass('disabled') || $('.ctctp-block #send-campaign').hasClass('disabled')) {
			return;
		}

		$(e.target).addClass('disabled');
		$('.ctctp-block #campaign-status').html('').hide();

		spinner = $( '#send-campaign-spinner' );
		spinner.css('visibility', 'visible');

		var data = {
				'action': 'ctctp_send_campaign',
				'campaign_id': $('.ctctp-block input#campaign_id').val(),
				'is_preview': $(e.target).attr('id') == 'preview-campaign'
			};

		$.post(
			ajaxurl,
			data,
			function( response ) {
				spinner.css('visibility', 'hidden');
				$('.ctctp-block #campaign-status').html(response).show();

				$(e.target).removeClass('disabled');
			}
		);
	});

	$('.ctctp-block').on( 'click', '.row-actions .delete a', function(e) {

		e.preventDefault();

		var data = {
				'action': 'ctctp_delete_item_ajax',
				'campaign_id': $(this).data('campaign_id'),
				'item_id': $(this).data('item_id'),
				'type': $(this).data('type')
			};

		$.post(
			ajaxurl,
			data,
			function( response ) {
				location.reload();
			}
		);
	});

	$( window ).resize(function() {
		resizeTable();
	});

	function resizeTable() {

		var modal = $('#find-posts');
		if (modal.length > 0) {

			var hHeader = 37; 
			var hPaging = 46;
			var hThead = 35;
			var hBottom = 46;
			var hAdditional = 10;
			var hSub = hHeader + hPaging + hThead + hBottom + hAdditional;

			var hTbody = (modal.height() - hSub > 100) ? modal.height() - hSub : 100; 
			var tBody = modal.find('tbody#the-list');
			if (tBody.length > 0) {
				tBody.height(hTbody);
			}
		}
	}

	$('.ctctp-block').on('click', '.is_featured_post', function(e) {

		e.preventDefault();

 		// if ($('.ctctp-block').find('.featured .spinner:visible')) {
 		// 	return;
 		// }

		var post_id = $(this).find('span.featured').data('id');
		var data = {
				'action': 'ctctp_set_top_event',
				'campaign_id': $('.ctctp-block input#campaign_id').val(),
				'post_id': post_id
			};

		var $post = $(this).find('span.featured');

 		spinner = $post.find('.spinner' );
    	spinner.show();

		$.post(
			ajaxurl,
			data,
			function( response ) {

	    		var current_featured = $('.featured.dashicons.dashicons-yes');
	    		current_featured.removeClass('dashicons').removeClass('dashicons-yes');
		
				spinner.hide();
				$post.addClass('dashicons').addClass('dashicons-yes');
			}
		);
		
	});	

	$('.ctctp-block').on('click', '.is-reminder', function(e) {

//		e.preventDefault();

		var post_id = $(this).data('id');
		var is_reminder_post = $(this).find('input[type="radio"]:checked').val();
		var data = {
				'action': 'ctctp_set_reminder_event_ajax',
				'campaign_id': $('.ctctp-block input#campaign_id').val(),
				'post_id': post_id,
				'is_reminder_post': is_reminder_post
			};

		$.post(
			ajaxurl,
			data,
			function( response ) {
				//document.location.reload();
			}
		);
		
	});	
});
