<?php
/**
 * Plugin Name: Constant Contant Posts
 * Plugin URI: 
 * Description: This plugin adds ability to export posts to Constant Contact.
 * Version: 1.0.0
 * Author: 
 * Author URI: 
 * License: 
 */

register_activation_hook( __FILE__, 'ctctp_install_db' );

if ( is_admin() ) {

    require_once('class.constant-contacts-posts.php');
    ConstantContactsPosts::getInstance();
}

function ctctp_install_db() {

    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();

    $campaigns_table_name = $wpdb->prefix . 'ctctp_campaigns';
    $campaign_posts_table_name = $wpdb->prefix . 'ctctp_campaign_posts';
    $campaign_events_table_name = $wpdb->prefix . 'ctctp_campaign_events';

    $sql_campaigns = "CREATE TABLE $campaigns_table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    name varchar(255) not null,
    introduction text,
    creation_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
    is_filled_posts int(1) DEFAULT 0 NOT NULL,
    is_filled_events int(1) DEFAULT 0 NOT NULL,
    is_filled_reminders int(1) DEFAULT 0 NOT NULL,
    is_sent int(1) DEFAULT 0 NOT NULL,
    UNIQUE KEY id (id)
    ) $charset_collate;";

    $sql_posts = "CREATE TABLE IF NOT EXISTS $campaign_posts_table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    campaign_id mediumint(9) not null,
    post_id mediumint(9) not null,
    post_title text not null,
    post_content text not null,
    post_date datetime not null,
    is_custom_post int(1) DEFAULT 0 NOT NULL,
    is_featured_post int(1) default 0 not null,
    is_reminder_post int(1) default 0 not null,
    UNIQUE KEY id (id)
    ) $charset_collate;";

    $sql_events = "CREATE TABLE IF NOT EXISTS $campaign_events_table_name (
    id mediumint(9) NOT NULL AUTO_INCREMENT,
    campaign_id mediumint(9) not null,
    event_id mediumint(9) not null,
    event_title text NULL,
    event_date varchar(50) NULL,
    event_image varchar(255) NULL,
    is_custom_event int(1) DEFAULT 0 NOT NULL,
    UNIQUE KEY id (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql_campaigns );
    dbDelta( $sql_posts );
    dbDelta( $sql_events );

    add_option( "ctctp_version", "1.0" );
}

?>