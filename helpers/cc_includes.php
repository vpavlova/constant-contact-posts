<?php

	global $campaign_item;

    use Ctct\ConstantContact;
    use Ctct\Components\Contacts\ContactList;
    use Ctct\Components\EmailMarketing\Campaign;
    use Ctct\Components\EmailMarketing\MessageFooter;
    use Ctct\Components\EmailMarketing\Schedule;
    use Ctct\Exceptions\CtctException;

    $cc = new ConstantContact(get_option('ctct_api_key'));

    $campaign = new Campaign();
    $campaign->name = $campaign_item['name'] . ' ' . date("F j, Y, g:i a");
    $campaign->subject = $campaign_item['name'];
    $campaign->from_name = get_option('ctct_from_email');
    $campaign->from_email = get_option('ctct_from_email');
    $campaign->greeting_string = "";
    $campaign->reply_to_email = get_option('ctct_from_email');
    $campaign->text_content = strip_tags($campaign_item['content']);
    $campaign->email_content = $campaign_item['content'];
    $campaign->email_content_format = "HTML";

    try {
        $result = $cc->addEmailCampaign(get_option('ctct_access_token'), $campaign);

        global $wpdb;
        $campaigns_table_name = $wpdb->prefix . 'ctctp_campaigns';
        $result = $wpdb->update($campaigns_table_name, array('is_sent' => 1), array('id' => $campaign_item['id']));

        echo '<div class="updated"><p>Campaign is prepared for sending</p></div>';
    }
    catch(Exception $e) {
        $errors = $e->getErrors();
        if (is_array($errors)) {
            echo $errors[0]['error_message'];
        }
        else {
            echo $errors;
        }
    }
?>